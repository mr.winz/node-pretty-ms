#!/bin/bash

## Save current working dir
PROJECT_ROOT=$(pwd)

EXIT_STATUS=0

BACKUP_DIR=$AUTOPKGTEST_TMP/some_backups
mkdir -p "$BACKUP_DIR"

## Expected plugin path after git clone
PLUGIN_DIR=$AUTOPKGTEST_TMP/yarn-plugin-apt_build_plugin
mkdir -p "$PLUGIN_DIR"
PLUGIN=$PLUGIN_DIR/plugin-apt.js

## Files liable to modification
files=(
  "$PROJECT_ROOT/.yarn"
  "$PROJECT_ROOT/.pnp.cjs"
  "$PROJECT_ROOT/.yarnrc.yml"
  "$PROJECT_ROOT/package.json"
  "$PROJECT_ROOT/yarn.lock"
)

do_backup() {
  local expected_backup_files=("$@")

  for expected_backup_file in "${expected_backup_files[@]}"; do
    # File exists
    if [ -e "$expected_backup_file" ]; then
      # Extract the filename from the file path
      filename=$(basename "$expected_backup_file")

      # Copy the file to the backup directory
      cp -r "$expected_backup_file" "$BACKUP_DIR/$filename"

      echo "Backed up file: $expected_backup_file"
    fi
  done
}

do_restore() {
    local expected_backup_files=("$@")
    # Restore backups
    for expected_backup_file in "${expected_backup_files[@]}"; do
        backup_file="$BACKUP_DIR/$(basename "$expected_backup_file")"
        
		if [ -e "$expected_backup_file" ]; then
            rm -rf "$expected_backup_file"
        fi
        
        # File exists
        if [ -e "$backup_file" ]; then
            # Restore the backup
            cp -r "$backup_file" "$expected_backup_file"
            
            echo "Restored: $expected_backup_file from $backup_file"
        fi
    done

    if rm -rf $BACKUP_DIR; then
		echo "Removed $BACKUP_DIR"
	fi
}

## Back up some files that might change
do_backup "${files[@]}"

## Change cwd to download plugin outside workspace
cd $AUTOPKGTEST_TMP

## Download the plugin
wget -O $PLUGIN -o /dev/null https://salsa.debian.org/mr.winz/yarn-plugin-apt_build_plugin/-/raw/main/plugin-apt.js?ref_type=heads&inline=false

## Change cwd back to project root
cd "$PROJECT_ROOT"

## Set Yarn version to berry
yarnpkg set version berry

## Import it into your project
yarnpkg plugin import $PLUGIN

## Run test 
if yarnpkg apt-install --local; then
  echo "Test succeeds"
else 
  EXIT_STATUS=0082
fi

## Restore some files that might have changed
do_restore "${files[@]}"

exit $EXIT_STATUS

